## gulp-swig 页面不刷新 (视频: Gulp 案例 - 页面模板编译)

编号: 001

现象: 修改源文件后, 浏览器不能及时看到

原因: 浏览器会有缓存, 开启缓存导致

解决办法: 

```js
// 页面任务
const page = () => {
    return src('src/**/*.html', { base: 'src' })
         // 传递渲染数据  默认不缓存
            .pipe(swig({ data, defaults:{ cache: false } }))  
            .pipe(dest('dist'))
}
```



------



## html-loader标签属性attrs (视频: Webpack 加载资源的方式)

编号: 002

现象: 用 attrs 指定属性报错

原因: 代码包升级. webpack中文手册未能及时更新

解决办法:

**参考:**

英文官网

https://webpack.js.org/loaders/html-loader/

npm 官网手册

https://www.npmjs.com/package/html-loader

**直接代码参考:**

```js
module.exports = {
  module: {
    rules: [
      {
        test: /\.html$/i,
        loader: 'html-loader',
        options: {
          attributes: {
            list: [
              // All default supported tags and attributes
              '...',
              {
                tag: 'img',
                attribute: 'data-src',
                type: 'src',
              },
              {
                tag: 'img',
                attribute: 'data-srcset',
                type: 'srcset',
              },
            ],
            urlFilter: (attribute, value, resourcePath) => {
              // The `attribute` argument contains a name of the HTML attribute.
              // The `value` argument contains a value of the HTML attribute.
              // The `resourcePath` argument contains a path to the loaded HTML file.


              if (/example\.pdf$/.test(value)) {
                return false;
              }


              return true;
            },
            root: '.',
          },
        },
      },
    ],
  },
};
```



------



## copy-webpack-plugin (视频: Webpack 插件使用总结)

编号: 003

现象: 报错. 不能把文件复制到指定目录

原因: 插件升级, 配置项写法改变

解决办法:

查阅 webpack 英文官网

https://webpack.js.org/plugins/copy-webpack-plugin/

**直接参考代码:**

```js
const CopyPlugin = require('copy-webpack-plugin');


module.exports = {
    // ...


    plugins: [
        new CopyPlugin({      
            patterns: [
                { from: 'source', to: 'dest' },
                { from: 'other', to: 'public' },
            ],      
            options: {        
                concurrency: 100,
            },
        }),
    ],


    // ...
}
```



------



## 图片显示为[object Module]

编号: 004

现象:

![img](images/202012140030.png)

原因: Webpack配置项默认会按照 ES Module 处理, 但是插件一般都是按 CommonJS 来处理

解决办法:

![img](images/202012140031.png)

设置 esModule: false, 表示不按 ES Module 处理, 那么自然就是 CommonJS 了



------



## vue-style-loader 与 style-loader 用后者

编号: 005

现象: vue-style-loader与css-loader结合使用时报错.

原因: 新版本 css-loader 默认为ES Module方式, 而vue-style-loader版本旧只能按CommonJS方式处理

**解决办法1:** (**推荐**) 直接用 style-loader 代替 vue-style-loader

**解决办法2:** 配置 css-loader 见下图.

![img](images/202012140032.png)



------



## 已配置 html-loader , html-webpack-plugin 模板语法失效

编号: 006

现象: 在配置了 html-loader 处理 .html 文件后, 使用 html-webpack-plugin 设置模板文件为 index.html, 打包后模板文件中的变量并没有被替换解析.

原因: html-loader 先把 index.html 文件转换JS模块, html-webpack-plugin 工作时也经没有未处理的index.html 文件了. 这是二者同时工作带来的问题.

解决办法1: 

1) template 配置位置改为 index.ejs

2) index.html 文件本身改为 index.ejs

解决办法2:

配置 html-loader 

配置 include ,  仅对指定目录进行处理操作

配置 exclude , 不对指定目录进行处理操作



------



## 在 html-webpack-plugin 中, 删除 dist 后, 没有生成 index.html

编号: 007

现象: 修改其它文件后重新打包, html-webpack-plugin 没有生成 index.html

原因: 没有监测到 index.html 修改, 所以 html-webpack-plugin 不会重新工作生成 index.html

解决办法: 

在 html-webpack-plugin 的配置中, 添加 cache: false ,表示不进行缓存即可



------



## 控制台报错信息为 property of null 

编号: 008

现象:

原因: 没有 exclude, 或没有 presets: ['...']

解决办法:

在 webpack.config.js 配置文件中对 babel-loader 配置完整

```js
module.exports = {
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,  // 用include指定要处理的目录效率更高
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env']
                    }
                }
            }
        ]
    }
}
```



------



## vue-cli-service 不是内部或外部命令, 也不是可运行的程序或批处理文件

编号: 009

现象:

原因:

解决办法:

删除 node_modules 目录, 重新安装:

```
npm install
```



------



## Bash: /usr/local/bin/module-demo: Permission denied 权限受拒

编号: 010

现象:

![img](images/202012140033.png)

原因: 实际上执行的是 lib/cli.js , 该文件没有执行权限

解决办法:

```
sudo chmod 777 ./lib/cli.js
```



------



## HtmlWebpackPlugin 打包后文件找不到, 报404

编号: 011

现象: 页面上引入文件地址不对

![img](images/202012140034.png)

原因: 对 HtmlWebpackPlugin 中 publicPath 理解错误

解决:

把打包后运行的目录做为 . (当前目录) 或者 / (根目录) , 再去设置文件想到这个位置的路径



------



## npm publish 发包报 403 错误

编号: 012

现象:

![img](images/202012140035.png)

原因:

1) 注册账号后, 没有验证激活?

2) 是否注册, 是否 npm login 登录?

3) 是否为原来仓库地址, 还是淘宝镜像? 

4) 包名是否重复冲突?

解决办法:

根所上面原因分析做相应处理

获取当前地址  npm config get registry

恢复原有地址  npm config set registry=http://registry.npmjs.org

设置淘宝镜像  npm config set registry=https://registry.npm.taobao.org/





## 不是内部或外部命令

编号: 013

现象:

![2020-12-14_175743](images/2020-12-14_175743.png)

原因:

系统找不到相应的命令,  不知道命令文件在哪里

解决办法:

**获取命令所在目录位置, 配置到环境变量 Path 中** 

![2020-12-14_181242](images/2020-12-14_181242.png)



![2020-12-14_185602](images/2020-12-14_185602.png)

配置环境变量就是这个套路.

**重新打开 CMD**  再次执行命令即可



## yeoman 报错

编号: 014

现象: 

原因: 

**少写了一个 s** 

![2021-01-05_105633](images/2021-01-05_105633.png)



## yarn publish 第一次输入用户名的时候输错了

编号：015

现象：第一次执行 yarn publish 输入用户名时输错了， 以后只让重新输入密码，不再询问用户名。

原因：

解决办法：

**yarn  logout**

直接执行退出动作，yarn 会清除以前输入的登录信息，包括用户名账号密码，然后就可以输入正确的账号密码了。



## test 目录 yarn  link 后命令不执行

编号：016

现象：参照视频，创建 test 目录，创建 lib / cli.js 文件，yarn link 后， 执行 test 没有反应

原因：系统中可能有 test 内部命令。产生冲突。

解决办法：

**删除 test 目录。使用其它名称重新做，例如：my-test**

修改目录名称 + package.json 中包名  也可以

　

## vue-loader 未同时安装，使用时报错

编号：017

现象：未截图。

原因：你应该将 `vue-loader` 和 `vue-template-compiler` 一起安装。

解决办法：

```shell
npm install -D vue-loader vue-template-compiler   # 同时安装两个包
```

参考地址：https://vue-loader.vuejs.org/zh/guide/#vue-cli

　

## 编译 vue 项目后 css 代码未生效

编号：018

现象：

原因：

解决办法：

**去掉 package.json 文件中下面代码**

```json
"sideEffects": [
    "src"
 ],
```



## yo  sample  报错

编号：019

现象：

![20210224155004](images/20210224155004.png)

原因：

yeoman-generator 版本高

解决办法：

#### 方案1：

```shell
# 卸载当前版本
npm uninstall yeoman-generator

# 安装低版本的包
npm i yeoman-generator@4.13.0

# 执行
yo sample
```

#### 方案2：

```shell
# 全局安装模块
npm i -g yeoman-environment

# 新的执行方式(yoe没有打错)
yoe run sample
```



## Vue Generator 报错 (同上)

编号：020

现象：

原因：

解决办法：



## 通过 husky 使用   Git  Hooks   不生效

编号：021

现象：

**并没有执行预定的 eslint 之类的命令**

原因：

**husky 版本升级, 使用方法改变**

解决办法：

```
npm i husky@4.3.8 -D
```



