## 9.flow 中文路径报错

编号：

现象：

![20210112131145](images/20210112131145.png)

原因：路径中出现了中文字符

解决方案：

- 中文改成英文。
- 如果计算机账户名为中文，那么就需要换个不是中文的目录。
- **程序编写时，任何地方都避免使用中文是程序员的基本素养。**



## 8.手写 Promise 时报错 status = PENDING

编号：

现象：

![20210112122317](images/20210112122317.png)

原因：node 版本低，类中语法不认。

解决办法：**升级到 14.15.3** 

　

## 7.箭头函数

```js
// 普通形式
function test (a, b) {
  return a + b
}

// 1.箭头函数: 省略 function   函数保存到 fn 变量中
const fn1 = (a, b) => {
  return a + b
}

// 2.箭头函数: 省略 return , 表示a+b作为返回值 注意不能有 { } 
const fn2 = (a, b) =>  a + b 

// 3.箭头函数: 省略 参数两边小括号, 只有一个形参时才可以 
const fn3 = x =>  x + 7 
```

**更为主要的是箭头函数解决了一些场景下的 this 问题:**

没有自己的`this`，`arguments`，`super`或`new.target`

箭头函数不会创建自己的 this, 它只会从自己的作用域链的上一层继承 this

```js
function Person(){
  this.age = 0;

  setInterval(() => {
    this.age++; //  this 正确地指向 p 实例
  }, 1000);
}

var p = new Person();
```

```js
function Person(){
  this.age = 0;

  setInterval(function() {
    this.age++; //  this 会指向 window 对象
  }, 1000);
}

var p = new Person();
```

**参考更多细节:**

https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Functions/Arrow_functions

　

## 6. call、apply、bind 函数

```js
let user1 = {name: '小明', age: 18}
let user2 = {name: '小李', age: 17}
let user3 = {name: '小马', age: 16}

// 1.改变 this 指向
function test() {console.log( this.name )}
test.call(user1)   // this指向user1  输出 '小明'
test.apply(user2)  // this指向user2  输出 '小李'
let fn = test.bind(user3) // this指向user3 不直接执行
fn()               // 后续可调用执行   输出 '小马'

// 2.关于参数
function xxxx(a, b){console.log(a + b)}
xxxx.call(null, 3, 4)    // 参数依次书写   输出 7
xxxx.apply(null, [3, 4]) // 参数放在数组中 输出 7
let foo = xxxx.bind(null)
foo(3, 4)                // 输出 7

// 3.bind 可以先绑定一个参数, 返回一个新函数
let yoo = xxxx.bind(null, 3) 
yoo(4)  // 输出 7
```

　

## 5.reduce 方法

**参考:** https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Array/Reduce

```js
const arr = [1, 2, 3, 4]

// 求数组元素值的总和
let total = arr.reduce((tmp, item) => {
              return tmp + item
            }, 0)

console.log(total)
/*
  参数1: 回调函数
        回调函数参数
           tmp:  用来保存临时结果
           item: 数组中每一个元素值
           
  参数2: 初始值
        设置 tmp 最开始的初始值. 如果没有这个参数设置. tmp 将使用数组中第一个元素
*/
```



　

## 4.ES6 语法中 对象简写形式

```js
let  name = '张三'
let  sex = '女'
let  age = 18

const user1 = {
  name: name,    // 键: 值(变量) 形式
  sex: sex,
  age: age,
  say: function() {console.log('我找对象')}
}

// 简写形式
const user2 = {
  name,
  sex,
  age,
  say(){console.log('我找对象')}
}
```



　

## 3.promise 对象的 then 方法

**参考:**  

https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Promise/then#%E5%8F%82%E6%95%B0

then 方法中默认有两个函数做为参数  then( onResolved, onRejected )

then 方法中如果不是函数, 会硬性替换为 x=>x  当作onResolved    比如  .then(123)  替换为  .then(x=>x) 

**所以, Promise.resolve(1).then(2).then(Promise.resolve(3)).then(console.log)  输出的是 1**



　

## 2.为什么组合函数更倾向于从左到右?

```
我们现在写代码时, 如果一个函数的返回值作为另一个函数的参数,会是这样

fun1(   fun2(   fun3()  )     )

执行的先后顺序写会是  fun1() <-- fun2() <-- fun3()
从上面这样看, 很自然的就是  从右到左了 
```



## 1.once 函数

```js
function once(fn) {
    let done = false
    console.log(1)
    return function(){
        if(!done) {
            done = true
            return fn.apply(this, arguments)
        }
    }
}

// 第1次调用once: 
// 执行了console.log(1)              --------  输出 1
// 返回了一个函数保存到 pay 中
let pay = once(function(money){
    console.log(`支付：${money} RMB`)
})

// 第2次调用once:
// 执行了console.log(1)              --------- 输出  1
once(function(money){
    console.log(`支付: ${1} RMB`)
})() // 返回了一个函数, 返回的函数被直接调用了 ---- 输出  支付: 1 RMB

// 第3次调用once:
// 执行了console.log(1)              --------- 输出  1
once(function(money){
    console.log(`支付: ${2} RMB`)
})() // 返回了一个函数, 返回的函数被直接调用了 ---- 输出  支付: 2 RMB


pay(5)  // 调用第一次调once时返回的pay  ------ 输出  支付: 5 RMB   在pay中把done赋值为true
pay(6)  // 调用第一次调once时返回的pay  ------ 无输出, 直接返回 因为 if(!done) 是false
pay(7)  // 调用第一次调once时返回的pay  ------ 无输出, 直接返回 因为 if(!done) 是false
```

**上面代码相当于:**

```js
let pay1 = once(function(money){
    console.log(`支付：${money} RMB`)
})

let pay2 = once(function(money){
    console.log(`支付: ${1} RMB`)
})
pay2()

let pay3 = once(function(money){
    console.log(`支付: ${2} RMB`)
})
pay3()

pay1(5)  
pay1(6) 
pay1(7)  
```



