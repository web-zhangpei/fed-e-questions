# Node.js 基础

## 安装

　

#### 1.下载 node

- **中文网址下载（快）: **  http://nodejs.cn/download/
- **英文网址下载（慢）：**https://nodejs.org/zh-cn/download/

**说明**

- 推荐使用 **LTS** (long term support) 长期支持版本，现在 **14.15.3**
- 安装时一路 next 就可以安装完成。npm 会一同安装
- **npm** 是 node 的**包管理工具**，用来下载，安装在node环境下运行的软件包

　

#### 2.配置镜像

国外服务器不方便访问，设置镜像后就可以从国内服务器下载软件包

```shell
# 设置以后下载时,自动从淘宝下载
npm config set registry http://registry.npm.taobao.org
```

特殊情况, 切换回原有地址

```shell
npm config set registry https://registry.npmjs.org
```

查看当前地址设置

```shell
npm config get registry
```

　

#### 3.npm 下载软件包

练习使用 npm 下载安装 node 环境下运行的软件包

npm 下载的软件包会存储在 **node_modules** 目录中

```shell
npm i h2otest -D   # 安装一个包 h2otest 练习测试

# i 是 install 的简单 
# -D 把下载的软件包视为开发依赖, 记在package.json的devDependencies(开发时才使用的)
# -S 把下载的软件包视为生产依赖, 记在package.json的dependencies(写完项目后,代码执行时要用的, 默认)
# -g 全局安装(安装在系统上npm的一个总的目录中), 不加会默认安装在当前目录的 node_modules 
```

　

#### 4.安装 cnpm

当配置淘宝镜像也无法下载软件包时, cnpm 有奇效~  所以先安装, 备用

```shell
# 通过 npm 安装 cnpm
npm install -g cnpm --registry=https://registry.npm.taobao.org
```

　

#### 5.其它命令

```shell
npm  root  -g   # 全局模块目录  一般为 C:\Users\Administrator\AppData\Roaming\npm\node_modules
npm  bin   -g   # 全局命令目录  一般为 C:\Users\Administrator\AppData\Roaming\npm

# 缓存清理  直接删除 node_modules 也还是可能有缓存
npm   cache  clean  --force

# 查看 .npmrc 文件内容
npm config ls -l   # 其中包含 .npmrc 文件位置
npm config ls      # 查看主要内容

# 直接编辑 .npmrc 文件
npm config edit
```





## 执行 JS 文件

#### 简单  JS 文件

```shell
node abcd.js
```



#### 包文件的执行

```shell
# 创建工作目录，并进入
mkdir mytest
cd mytest

# 生成 package.json
npm init -y

# 安装一个测试包 h2otest 
npm i h2otest -D
```

　

**1.使用 npx 执行**

```shell
npx h2otest   # 输出字符串 stay hungry, stay foolish! 
```

- npx 将尝试查找 node_modules / h2otest  中的可执行文件
- 如果当前目录没有 node_modules , 它会继续向上查找, 直到磁盘根目录

　

**2.通过在 package.json 中添加脚本命令执行**

编辑  package.json 

```json
"scripts": {
  "xxx": "h2otest"
},
```

在命令行执行

```shell
npm  run  xxx
```

此方法为常见套路

　

**3.依然使用 node 执行. ( 不推荐 )**

```shell
c:\mytest> node ./node_modules/h2otest/bin/index.js
```

正因为这样比较麻烦, 才有上面两种执行方法

　

## 使用 nodemon

nodemon 可以监视文件修改. 被它监视的文件，修改保存后，就会被执行一次。

#### 安装

```shell
# 创建项目目录
mkdir mytest

# 进入目录
cd mytest

# 初始化。生成 package.json
npm init -y

# 全局安装 nodemon 包
npm i nodemon -g
```

　

#### 使用

```shell
# 通过 npx 运行 nodemon, 监视 abcd.js 的改变   ctrl + c 结束
npx nodemon abcd.js
```



## 使用 webpack-dev-server

( 此方法略复杂. 可以尝试. 也可以用上面的 nodemon~ )

**要安装 4 个东东:**

- webpack-dev-server  让代码以服务器为环境运行,调试
- webpack   做为代码打包编译的核心, 也是webpack-dev-server 的基础, 必须安装
- webpack-cli  用来分析我们敲出来的 webpack 相关命令, 必须安装
- html-webpack-plugin  它可以帮助我们自动生成一个 index.html 文件

#### 安装

```shell
# 创建目录,进入目录
mkdir mytest
cd mytest
npm init -y

# 同时安装多个软件包 这里安装的都是相对新的版本
npm i webpack webpack-cli webpack-dev-server -D 

# 安装下面插件, 自动生成一个index.html文件
npm i html-webpack-plugin -D
```

　

#### 配置

**创建 webpack.config.js**     默认的配置文件名

```js
const HtmlWebpackPlugin = require('html-webpack-plugin')
module.exports = {
  mode: 'none',
  stats: 'none',
  devtool: 'source-map',
  plugins: [
    new HtmlWebpackPlugin()
  ]
}
```



#### 使用

**1.编写一个学习文件 xxx.js**

```js
console.log('i love you')
```

**2.运行观察**

```shell
# 在浏览器中打开html页面, 其中引入了编译之后.js文件
npx webpack serve --entry ./xxx.js --open
```

