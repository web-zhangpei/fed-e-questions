# part3-常见问题及解决办法



## 执行parcel报错 (视频: 导入Snabbdom) 

编号：014

现象：

![20210302172839](images/20210302172839.png)

![20210302172949](images/20210302172949.png)

原因：

parcel 升级到 1.12.4，内嵌的 babel 版本升级了，导致该问题出现

参考：https://github.com/parcel-bundler/parcel/issues/5943

解决办法：

```shell
npm  i  parcel-bundler@1.12.3  -D
```



## snabbdom导入报错(视频: 导入Snabbdom)

编号: 001

现象: 提示语法错

原因: 近期 Snabbdom 版本更迭很快. 新旧版本导入方式不同.

解决办法: 

此部分课程, 只为让同学们理解原理, 为了能和视频同步. 强烈建议安装指定版本:

```
npm  i  snabbdom@v0.7.4
```

如果使用 v1.x/v2.x 的话，导入 init、h，以及模块只要把路径补全即可。

```js
import { h }    from 'snabbdom/build/package/h'
import { init } from 'snabbdom/build/package/init'
import { classModule } from 'snabbdom/build/package/modules/class'
```

**Github issue**

https://github.com/snabbdom/snabbdom/issues/723



------



## 浏览器不显示 source map 文件夹(视频: 准备工作-调试)

编号: 002

现象: 开发者工具 sources 项中, 看不到 src 目录

原因: 除了package.json中配置, 浏览器中也要开启

解决办法:

第一步

![img](images/202012140001.png)

第二步

![img](images/202012140002.png)

第三步

![img](images/202012140003.png)



------



## Snabbdom 中 h() 函数里绑定事件时传参问题

编号: 003

现象: 根据 2.x 版本样例, 事件传参不可用

原因: 样例代码是旧的

解决办法:

**官方报错代码:**

![img](images/202012140004.png)

**写法1:**

![img](images/202012140005.png)

**写法2:**

![img](images/202012140006.png)

**写法3:**

```js
on: {
   click: changeSort.bind(this, 'rank')
}
```



------



## Github 自动化部署时报错, 报 npm、pm2  command not found (命令找不到)

编号：004

现象：在服务器命令行执行命令正常. 自动化部署时却报 command not found

原因：(推测) 当前用户环境的 路径配置可以, 自动部署时不能用到当前配置

**现象截图**

![img](images/202012140007.png)

![img](images/202012140008.png)

解决办法:

**1.获取 node、npm、pm2 的运行路径**

```
whereis node
whereis npm 
whereis pm2
```

![img](images/202012140009.png)

![img](images/202012140010.png)

**2.根据返回结果，创建软连接（类似于windows中的快捷方式）**

```
sudo ln -s /root/.npm-global/bin/pm2  /usr/bin/pm2
sudo ln -s /usr/local/node/bin/npm  /usr/bin/npm
sudo ln -s /usr/local/node/bin/node /usr/bin/node
```

**备注: 通过 PATH 设置未成功 (估计是因为没有重启服务器)**

```
# 1.编辑文件
vim /etc/profile


# 2.在最后一行追加下面内容
# npm 和 node 所在目录都是 /usr/local/node/bin，所以只写一个
export $PATH=$PATH:/usr/local/node/bin:/root/.npm-global/bin
# ESC :wq  回车   保存


# 3.加载新配置
source /etc/profile


在当前服务器命令行查看 PATH 是修改后的。可能会有重复路径。在新shell中打开就没有了。
但是，推测，没有重启服务器，此时仓库服务器部署就拿不到当前路径配置。当然也有可能是账号不同，以后细查~
```



------



## snabbdom.ts:81 Uncaught ReferenceError: parcelRequire is not defined

编号:  005

现象:  snabbdom 样例代码报错

原因:  html 文件引入 js 文件的标签, 添加了 type="module"

解决办法:

**去掉 html 中  type="module"**

*|*![img](images/202012140011.png)

![img](images/202012140012.png)



------



## 服务器端 打包 部署 , 提示被杀死 kill

编号: 006

现象:

![img](images/202012140013.png)

原因:  推测  内存不足

解决办法:

**1)查看当前内存情况**

![img](images/202012140014.png)

**2)参考解决办法**

![img](images/202012140015.png)

参考网址: https://www.cnblogs.com/yizijianxin/p/10634214.html



------



## 自动化部署报错 connect: connection refused

编号: 007

现象:

![img](images/202012140016.png)

原因: 连接的相关信息有误

解决办法:

**1) 要同学发截图、main.yml**

逐一检查:  secrets.HOST  secrets.USERNAME   secrets.PASSWORD   secrets.PORT  

可能具体值错误, 也可能是单词拼写错误

![img](images/202012140017.png)

![img](images/202012140018.png)

![img](images/202012140019.png)

![img](images/202012140020.png)

**2) 检查 main.yml 中是否拼写错误**

![img](images/202012140021.png)

**3) 连接信息可以 更新 移除**

![img](images/202012140022.png)



------



## 自动化部署服务 ssh: handshake failed: ssh: unable to authenticate ...

编号: 008

现象:

![img](images/202012140023.png)

原因:

连接信息有误. 直接提示没有密码

解决办法:

**要同学发截图、main.yml**

逐一检查:  secrets.HOST  secrets.USERNAME   secrets.PASSWORD   secrets.PORT  

可能具体值错误, 也可能是单词拼写错误, **端口是 22**



------



## 自动化部署  tar -zcvf release.tgz 生成压缩包时报错

编号: 009

现象:

![img](images/202012140024.png)

原因: **仓库中缺少命令行中的一些文件, 导致压缩失败**

解决办法:

**本地添加相应文件, 生成新的 commit, 重新 push**



------



## 服务器部署 服务端执行 pm2  start  npm  --  start  报错

编号: 010

现象:

![img](images/202012140025.png)

原因:

1) pm2 start npm -- start  执行多次会造成端口冲突

2) pm2 log RealWorld 中有以前的错误信息.

解决办法:

用 pm2 reload npm -- start

删除 RealWorld-error.log 再测试



------



## 自动化部署正常  无法访问此网站

编号: 011

现象:

![img](images/202012140026.png)

原因:

**pm2.config.js 文件中代码写错**

![img](images/202012140027.png)

解决办法:

检查 pm2.config.js  相似问题也要检查 nuxt.config.js  



------



## scp 命令行中不能有中文

编号：012

现象：

原因：

解决办法：

**先把文件放在没有中文的路径目录中，然后再执行 scp 复制**

　

## realworld 部署环节报错

编号：013

现象：

原因：**git 仓库还是老师的地址, 不是学生自己的**

解决办法：



## 1.服务器上安装 MySQL

https://juejin.cn/post/6903877424484712455 

 

## 2.远程访问 MySQL

https://juejin.cn/post/6904234342575407111



## Gridsome 安装失败, 尝试使用 cnpm