## 前提

- **Strapi 本地可以打开使用 sqlite 文件数据库**
- **远程 MySQL 数据库版本 >= 5.6**
- **远程 MySQL 中已经创建一个可以远程连接的账号密码**

最好已安装 cnpm , 如果未安装 cnpm 可以执行下面命令:

```shell
npm install -g cnpm --registry=https://registry.npm.taobao.org
```



## 安装 mysql 包

这个包用来实现 js 代码连接 MySQL 数据库. 安装 Strapi 时用的 cnpm install, 要追加安装 mysql 包也要使用 cnpm

```
cnpm install mysql -S
```



## 创建数据库

> MySQL 实际是一个数据库管理系统. 真实的数据要存在一个个「数据库」当中. 所以,
>
> 首先我们要创建一个数据库, 这里我给它取名 lagou

**1.连接远程MySQL, 右键菜单中「新建数据库...」**

![2020-12-14_134833](images/2020-12-14_134833.png)

**2.设置相应的名称, 字符集**

![2020-12-14_154331](images/2020-12-14_154331.png)





## 修改配置文件

#### 1.复制

打开下面手册页面, 复制 mysql 相关配置代码

https://strapi.io/documentation/v3.x/concepts/configurations.html#database

![2020-12-14_144155](images/2020-12-14_144155.png)

**或者直接复制下面代码:**

```js
module.exports = ({ env }) => ({
  defaultConnection: 'default',
  connections: {
    default: {
      connector: 'bookshelf',
      settings: {
        client: 'mysql',
        host: env('DATABASE_HOST', 'localhost'),
        port: env.int('DATABASE_PORT', 3306),
        database: env('DATABASE_NAME', 'strapi'),
        username: env('DATABASE_USERNAME', 'strapi'),
        password: env('DATABASE_PASSWORD', 'strapi'),
      },
      options: {},
    },
  },
});
```



#### 2.粘贴

**覆盖  ./config/database.js  内容, 并添加实际信息**

```js
module.exports = ({ env }) => ({
  defaultConnection: 'default',
  connections: {
    default: {
      connector: 'bookshelf',
      settings: {
        client: 'mysql',
        host: env('DATABASE_HOST', '117.50.11.104'),   // 1.远程主机ip
        port: env.int('DATABASE_PORT', 3306),
        database: env('DATABASE_NAME', 'lagou'),       // 2.MySQL中已经创建好的数据库名称
        username: env('DATABASE_USERNAME', 'shui'),    // 3.连接账号 (之前文章中已添加)
        password: env('DATABASE_PASSWORD', 'js12345678abcd'), // 4.连接密码
      },
      options: {},
    },
  },
});
```



## 本地启动

```shell
npm run develop
```

![2020-12-14_154918](images/2020-12-14_154918.png)

再次弹出「注册页面」说明配置成功

![2020-12-14_155412](images/2020-12-14_155412.png)



