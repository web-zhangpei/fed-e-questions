## 前提与准备

**1.服务器系统 CentOS 8.2 64位 ( 非必须 )** 

**2.已通过 ssh 远程连接到服务器**

**3.当前用户为 root** 

**4.想了解更多, 或者下面的不明白, 可以看 https://lnmp.org/install.html**



## 安装 MySQL

#### **1.切换到家目录. ( 此步不做也行~ )**

```
cd 回车
```

#### **2.复制下面指令到命令行, 然后回车, 发呆即可**

```
wget http://soft.vpser.net/lnmp/lnmp1.7.tar.gz -cO lnmp1.7.tar.gz && tar zxf lnmp1.7.tar.gz && cd lnmp1.7 && ./install.sh db
```

> 此为后端常用的 lnmp 一键安装环境. 它先下载安装脚本, 然后根据命令参数下载源码安装包. 之后编译安装.
>
> 命令最后 db, 表示单独安装 MySQL 数据库

*1核2G 服务器安装时间大概需要 12 分钟*

![20201208_172000](images/20201208_172000.png)

#### **3.安装结束**

![20201208173141](images/20201208173141.png)

#### **4.连接测试**

![20201208_173548](images/20201208_173548.png)



#### MySQL 状态管理

```
/etc/init.d/mysql {start|stop|restart|reload|force-reload|status}

/etc/init.d/mysql start  # MySQL安装后默认就是运行状态, 所以一般不用执行这条指令

/etc/init.d/mysql stop      # 停止 MySQL 服务
/etc/init.d/mysql restart   # 重启
/etc/init.d/mysql reload    # 重新加载, 应该是比 restart 好一些
/etc/init.d/mysql status    # 查看当前状态
```

![20201208_183218](images/20201208_183218.png)



#### 卸载 MySQL

```
cd /root/lnmp1.7  回车
./uninstall.sh    回车
1 回车
任意键回车
```

![20201208_184806](images/20201208_184806.png)



## 安装 Nginx

```
wget http://soft.vpser.net/lnmp/lnmp1.7.tar.gz -cO lnmp1.7.tar.gz && tar zxf lnmp1.7.tar.gz && cd lnmp1.7 && ./install.sh nginx
```

**具体步骤自已悟~**



