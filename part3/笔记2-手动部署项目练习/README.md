# 手动部署练习

[**视频讲解**]()

## 1.本地跑通 / 打包构建

##### [a.下载练习用的代码](https://gitee.com/lagoufed/fed-e-questions/raw/master/part3/realworld-nuxtjs.zip)

https://gitee.com/lagoufed/fed-e-questions/raw/master/part3/realworld-nuxtjs.zip

练习用代码，也可以到课程资料中查找:

part3-3 课堂资料（ 03-03-study-materials / code / 3-3-4-1-nuxtjs综合案例 /realworld-nuxtjs ） 

![unzip](images/unzip.png)

![install](images/install2.png)



![rundev](images/rundev.png)



![rundev2](images/rundev2.png)



![tobuild](images/tobuild.png)



![builded](images/builded.png)

![runbuild](images/runbuild.png)





## 2.上传并执行

[**视频讲解**]()

![tozip](images/tozip.png)

| 名称              | 描述                     |
| ----------------- | ------------------------ |
| .nuxt             | 打包后的结果             |
| static            | 项目中用到的静态资源文件 |
| nuxt.config.js    | 项目配置文件             |
| package.json      | 项目描述文件             |
| package-lock.json | 已安装依赖描述文件       |
| pm2.config.json   | pm2运行配置文件          |

  

![upload2](images/upload2.png)



![unzip1](images/unzip1.png)



![installdep](images/installdep.png)



![runbuild1](images/runbuild1.png)



![pm21](images/pm21.png)



![pm2delete](images/pm2delete.png)



![pm2file](images/pm2file.png)



![pm2reload](images/pm2reload.png)



